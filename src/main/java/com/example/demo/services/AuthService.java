package com.example.demo.services;

import com.example.demo.models.Auth;
import com.example.demo.models.User;
import com.example.demo.repositories.AuthRepository;
import com.example.demo.repositories.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@AllArgsConstructor
@Service
public class AuthService {
    private final AuthRepository authRepository;
    private final UserRepository userRepository;
    private final UserService userService;

    public Auth register(Auth auth) throws Exception {
        User user = new User();
        if (userRepository.existsByName(auth.getLogin())) throw new Exception("Name is not unique");
        user.setName(auth.getLogin());
        userService.add(user);
        User userSaved = userRepository.getUserByName(auth.getLogin());
        auth.setUserId(userSaved.getId());
        return this.authRepository.save(auth);
    }

    public Auth login(Auth auth) throws Exception {
        Auth localAuth = authRepository.getAuthByLogin(auth.getLogin());
        if (!authRepository.existsByLoginAndPassword(auth.getLogin(), auth.getPassword())) throw new Exception("Bad login");

        localAuth.setToken(UUID.randomUUID().toString().toUpperCase());

        return this.authRepository.save(localAuth);
    }

    public Auth update(Auth auth, String token) throws Exception {
        if (!authRepository.existsAuthByToken(token)) throw new Exception("INVALID TOKEN");
        Auth localAuth = authRepository.getAuthById(auth.getId());
        auth.setUserId(localAuth.getUserId());
        return this.authRepository.save(auth);
    }

    public void delete(Auth auth, String token) throws Exception {
        if (!authRepository.existsAuthByToken(token)) throw new Exception("INVALID TOKEN");
        this.authRepository.delete(auth);
    }
}
