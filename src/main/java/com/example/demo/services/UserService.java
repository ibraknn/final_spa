package com.example.demo.services;

import com.example.demo.models.User;
import com.example.demo.repositories.AuthRepository;
import com.example.demo.repositories.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class UserService {
    private final UserRepository userRepository;
    private final AuthRepository authRepository;

    public List<User> getAll(String token) throws Exception {
        if (!authRepository.existsAuthByToken(token)) throw new Exception("INVALID TOKEN");
        return userRepository.findAll();
    }

    public User add(User user){
        return this.userRepository.save(user);
    }

    public User update(User user, String token) throws Exception {
        if (!authRepository.existsAuthByToken(token)) throw new Exception("INVALID TOKEN");
        return this.userRepository.save(user);
    }

    public void delete(User user, String token) throws Exception {
        if (!authRepository.existsAuthByToken(token)) throw new Exception("INVALID TOKEN");
        this.userRepository.delete(user);
    }
}
