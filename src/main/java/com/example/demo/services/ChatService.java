package com.example.demo.services;

import com.example.demo.models.Chat;
import com.example.demo.repositories.AuthRepository;
import com.example.demo.repositories.ChatRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class ChatService {
    private final ChatRepository chatRepository;
    private final AuthRepository authRepository;

    public List<Chat> getAll(String token) throws Exception {
        if (!authRepository.existsAuthByToken(token)) throw new Exception("INVALID TOKEN");
        return chatRepository.findAll();
    }

    public Chat add(Chat chat, String token) throws Exception {
        if (!authRepository.existsAuthByToken(token)) throw new Exception("INVALID TOKEN");
        return this.chatRepository.save(chat);
    }

    public Chat update(Chat chat, String token) throws Exception {
        if (!authRepository.existsAuthByToken(token)) throw new Exception("INVALID TOKEN");
        return this.chatRepository.save(chat);
    }

    public void delete(Chat chat, String token) throws Exception {
        if (!authRepository.existsAuthByToken(token)) throw new Exception("INVALID TOKEN");
        this.chatRepository.delete(chat);
    }
}
