package com.example.demo.services;

import com.example.demo.models.Message;
import com.example.demo.repositories.AuthRepository;
import com.example.demo.repositories.MessageRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class MessageService {
    private final MessageRepository messageRepository;
    private final AuthRepository authRepository;

    public List<Message> getAll(String token) throws Exception {
        if (!authRepository.existsAuthByToken(token)) throw new Exception("INVALID TOKEN");
        return messageRepository.findAll();
    }

    public Message add(Message message, String token) throws Exception {
        if (!authRepository.existsAuthByToken(token)) throw new Exception("INVALID TOKEN");
        return this.messageRepository.save(message);
    }

    public Message update(Message message, String token) throws Exception {
        if (!authRepository.existsAuthByToken(token)) throw new Exception("INVALID TOKEN");
        return this.messageRepository.save(message);
    }

    public void delete(Message message, String token) throws Exception {
        if (!authRepository.existsAuthByToken(token)) throw new Exception("INVALID TOKEN");
        this.messageRepository.delete(message);
    }
}


