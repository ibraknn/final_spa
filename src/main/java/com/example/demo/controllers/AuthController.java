package com.example.demo.controllers;

import com.example.demo.models.Auth;
import com.example.demo.models.User;
import com.example.demo.repositories.AuthRepository;
import com.example.demo.repositories.UserRepository;
import com.example.demo.services.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@Controller
@AllArgsConstructor
@RequestMapping(value = "/api/v1/auth")
public class AuthController {
    private final AuthService authService;
    private final UserRepository userRepository;
    private final AuthRepository authRepository;

    @PostMapping({"/register"})
    public ResponseEntity<?> register(@RequestBody Auth auth) throws Exception {
        return ResponseEntity.ok(authService.register(auth));
    }

    @PostMapping({"/login"})
    public ResponseEntity<?> login(@RequestBody Auth auth) throws Exception {
        return ResponseEntity.ok(authService.login(auth));
    }

    @PutMapping({"/update/{id}"})
    public ResponseEntity<?> update(@RequestBody Auth auth, @PathVariable Long id, @RequestHeader String token) throws Exception {
        Optional<Auth> authOptional = authRepository.findById(id);
        if (!authOptional.isPresent()) return ResponseEntity.notFound().build();
        auth.setId(id);
        authService.update(auth, token);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping({"/delete"})
    public ResponseEntity<?> delete(Auth auth, @RequestHeader String token) throws Exception {
        authService.delete(auth, token);
        return ResponseEntity.ok().build();
    }

}
