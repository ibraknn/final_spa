package com.example.demo.controllers;

import com.example.demo.models.Message;
import com.example.demo.models.User;
import com.example.demo.repositories.ChatRepository;
import com.example.demo.repositories.MessageRepository;
import com.example.demo.repositories.UserRepository;
import com.example.demo.services.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@Controller
@AllArgsConstructor
@RequestMapping(value = "/api/v1/message")
public class MessageController {
    private final MessageService messageService;
    private final MessageRepository messageRepository;
    private final UserRepository userRepository;
    private final ChatRepository chatRepository;

    @GetMapping({"/getAll"})
    public ResponseEntity<?> getAll(@RequestHeader String token) throws Exception {
        return ResponseEntity.ok(messageService.getAll(token));
    }

    @PostMapping({"/create"})
    public ResponseEntity<?> add(@RequestBody Message message,@RequestHeader String token) throws Exception {
        if (userRepository.existsById(message.getUserId()) &&
                chatRepository.existsById(message.getChatId())) {
            return ResponseEntity.ok(messageService.add(message, token));
        }
        return ResponseEntity.badRequest().build();
    }

    @PutMapping({"/update/{id}"})
    public ResponseEntity<?> update(@RequestBody Message message, @PathVariable Long id,@RequestHeader String token) throws Exception {
        Optional<Message> messageOptional = messageRepository.findById(id);
        if (!messageOptional.isPresent()) return ResponseEntity.notFound().build();
        message.setId(id);
        messageService.update(message, token);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping({"/delete"})
    public ResponseEntity<?> delete(@RequestBody Message message,@RequestHeader String token) throws Exception {
        messageService.delete(message, token);
        return ResponseEntity.ok().build();
    }
}
