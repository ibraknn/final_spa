package com.example.demo.controllers;

import com.example.demo.models.Chat;
import com.example.demo.repositories.ChatRepository;
import com.example.demo.services.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

@RestController
@Controller
@AllArgsConstructor
@RequestMapping(value = "/api/v1/chat")
public class ChatController {
    private final ChatService chatService;
    private final ChatRepository chatRepository;

    @GetMapping({"/getAll"})
    public ResponseEntity<?> getAll(@RequestHeader String token) throws Exception {
        return ResponseEntity.ok(chatService.getAll(token));
    }

    @PostMapping({"/create"})
    public ResponseEntity<?> add(@RequestBody Chat chat, @RequestHeader String token) throws Exception {
        Chat savedChat = chatService.add(chat, token);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(savedChat.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping({"/update/{id}"})
    public ResponseEntity<?> update(@RequestBody Chat chat, @PathVariable Long id, @RequestHeader String token) throws Exception {
        Optional<Chat> chatOptional = chatRepository.findById(id);
        if (!chatOptional.isPresent()) return ResponseEntity.notFound().build();
        chat.setId(id);
        chatService.update(chat, token);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping({"/delete"})
    public ResponseEntity<?> delete(Chat chat, @RequestHeader String token) throws Exception {
        chatService.delete(chat, token);
       return ResponseEntity.ok().build();
    }

}
