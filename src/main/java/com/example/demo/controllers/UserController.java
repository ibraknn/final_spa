package com.example.demo.controllers;

import com.example.demo.models.Chat;
import com.example.demo.models.User;
import com.example.demo.repositories.UserRepository;
import com.example.demo.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

@RestController
@Controller
@AllArgsConstructor
@RequestMapping(value = "/api/v1/user")
public class UserController {
    private final UserService userService;
    private final UserRepository userRepository;

    @GetMapping({"/getAll"})
    public ResponseEntity<?> getAll(@RequestHeader String token) throws Exception {
        return ResponseEntity.ok(userService.getAll(token));
    }

    @PostMapping({"/create"})
    public ResponseEntity<?> add(@RequestBody User user) {
        User savedUser = userService.add(user);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(savedUser.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping({"/update/{id}"})
    public ResponseEntity<?> update(@RequestBody User user, @PathVariable Long id, @RequestHeader String token) throws Exception {
        Optional<User> userOptional = userRepository.findById(id);
        if (!userOptional.isPresent()) return ResponseEntity.notFound().build();
        user.setId(id);
        userService.update(user, token);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping({"/delete"})
    public ResponseEntity<?> delete(User user, @RequestHeader String token) throws Exception {
        userService.delete(user, token);
        return ResponseEntity.ok().build();
    }
}
