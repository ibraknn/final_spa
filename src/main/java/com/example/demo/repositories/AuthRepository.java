package com.example.demo.repositories;

import com.example.demo.models.Auth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthRepository extends JpaRepository<Auth, Long> {
    Auth getAuthByLogin(String login);
    boolean existsByLoginAndPassword(String login, String password);
    boolean existsAuthByToken(String token);
    Auth getAuthById(Long id);
}
