drop table if exists auth;
create table auth (
    id serial,
    login varchar(64),
    password varchar(64),
    token varchar(64),
    user_id bigint
);

drop table if exists users;
create table users (
    id serial,
    name varchar(64)
);

drop table if exists chat;
create table chat (
                       id serial,
                       name varchar(64)
);

drop table if exists messages;
create table messages (
                      id serial,
                      user_id bigint,
                      chat_id bigint,
                      text varchar(255)
);

